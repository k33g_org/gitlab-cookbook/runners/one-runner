set -o allexport; source .env; set +o allexport

sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "${RUNNER_DESCRIPTION}" \
  --tag-list "docker,gitpod" \
  --run-untagged="true" \
  --locked="false"

